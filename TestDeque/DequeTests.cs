﻿using System;
using DequeSample;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace TestDeque
{
    [TestClass]
    public class DequeTests
    {
        MyDequeue<int> myDeque;
        MyDequeue<ComplexType<int>> deque;

        [TestInitialize]
        public void Initialize()
        {
            myDeque = new MyDequeue<int>();
        }

        [DataTestMethod]
        [DataRow(1, new int[] { 10, 20 })]
        [DataRow(-10, new int[] { 10 })]
        [DataRow(-10, new int[] { })]
        public void InsertFront_PopFront_Tests(int insValue, params int[] a)
        {
            myDeque = new MyDequeue<int>(a);
            myDeque.InsertFront(insValue);

            var result = myDeque.PopFront();

            Assert.AreEqual(result, insValue);
        }

        [DataTestMethod]
        [DataRow(1, 20, new int[] { 10, 20 })]
        [DataRow(-10, 10, new int[] { 10 })]
        [DataRow(-10, -10, new int[] { })]
        public void InsertFront_PopRear_Tests(int insValue, int expValue, params int[] a)
        {
            myDeque = new MyDequeue<int>(a);
            myDeque.InsertFront(insValue);

            var result = myDeque.PopRear();

            Assert.AreEqual(result, expValue);
        }

        [DataTestMethod]
        [DataRow(30, 10, new int[] { 10, 20 })]
        [DataRow(20, 10, new int[] { 10 })]
        [DataRow(40, 40, new int[] { })]
        public void InsertRear_PopFront_Tests(int insValue, int expValue, params int[] a)
        {
            myDeque = new MyDequeue<int>(a);
            myDeque.InsertRear(insValue);

            var result = myDeque.PopFront();

            Assert.AreEqual(result, expValue);
        }

        [DataTestMethod]
        [DataRow(30, new int[] { 10, 20 })]
        [DataRow(-10, new int[] { 10 })]
        [DataRow(-10, new int[] { })]
        public void InsertRear_PopRear_Tests(int insValue, params int[] a)
        {
            myDeque = new MyDequeue<int>(a);
            myDeque.InsertRear(insValue);

            var result = myDeque.PopRear();

            Assert.AreEqual(result, insValue);
        }

        [TestMethod]
        public void ComplexType_InsertFront_PopRear_Tests()
        {
            var insValue = -10;
            ComplexType<int>[] complexTypeList = new ComplexType<int>[]
            {
                new ComplexType<int>(10),
                new ComplexType<int>(20),
                new ComplexType<int>(30),
            };

            this.deque = new MyDequeue<ComplexType<int>>(complexTypeList);
            this.deque.InsertFront(new ComplexType<int>(insValue));
            var result = this.deque.PopFront();
            Assert.AreEqual(result.Id, insValue);
        }

        [TestCleanup]
        public void CleanUp()
        {
            this.deque = null;
            this.myDeque = null;
        }
    }

    public class ComplexType<TKey>
    {
        public TKey Id { get; set; }
        public string Name { get; set; }

        public ComplexType(TKey key)
        {
            this.Id = key;
        }
    }
}

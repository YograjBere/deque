﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DequeSample
{
    class Program
    {
        static void Main(string[] args)
        {
            MyDequeue<int> myDeque = new MyDequeue<int>(new int[] { 1, 2, 3, 34, 4, 5 });
            myDeque.Display();

            myDeque.InsertFront(0);
            myDeque.Display();

            myDeque.DeleteFront();
            myDeque.Display();

            myDeque.InsertFront(0);

            myDeque.DeleteRear();
            myDeque.Display();
        }
    }

    /// <summary>
    /// Defines a Deque data strucure
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class MyDequeue<T>
    {
        /// <summary>
        /// Head node of the deque
        /// </summary>
        private MyDoubleLinkedListNode<T> _head;

        #region ctor
        public MyDequeue()
        {

        }

        public MyDequeue(params T[] initialState)
        {
            createInitialState(initialState);
        }
        #endregion

        #region Operations

        /// <summary>
        /// Initializes the deque with initial values
        /// </summary>
        /// <param name="initialState"></param>
        public void Initialize(params T[] initialState)
        {
            createInitialState(initialState);
        }

        /// <summary>
        /// Inserts data at front of the deque
        /// </summary>
        /// <param name="data"></param>
        public void InsertFront(T data)
        {
            MyDoubleLinkedListNode<T> temp = _head;

            if (!isHeadNotInitialized(data))
            {
                MyDoubleLinkedListNode<T> newNode = new MyDoubleLinkedListNode<T>(data);
                temp.Prev = newNode;
                newNode.Next = temp;
                _head = newNode;
            }
        }

        /// <summary>
        /// Inserts the data at rear of the deque
        /// </summary>
        /// <param name="data"></param>
        public void InsertRear(T data)
        {
            if (!isHeadNotInitialized(data))
            {
                MyDoubleLinkedListNode<T> newNode = new MyDoubleLinkedListNode<T>(data);
                MyDoubleLinkedListNode<T> temp1 = this._head;

                //iterate to last node
                while (temp1.Next != null)
                {
                    temp1 = temp1.Next;
                }

                newNode.Prev = temp1;
                temp1.Next = newNode;
            }
        }

        /// <summary>
        /// Removed the data from front of the Deque
        /// </summary>
        public void DeleteFront()
        {
            MyDoubleLinkedListNode<T> temp = _head;
            if (temp.Next != null)
            {
                _head = temp.Next;
                temp.Prev = null;
            }
        }

        /// <summary>
        /// Deletes the data from rear of the deque
        /// </summary>
        public void DeleteRear()
        {
            MyDoubleLinkedListNode<T> temp = _head, prevNode = null;

            while (temp.Next != null)
            {
                temp = temp.Next;
            }

            if (temp.Prev != null)
            {
                prevNode = temp.Prev;
                prevNode.Next = null;
            }
            else
            {
                _head = null;
            }

            temp = null;
        }

        /// <summary>
        /// Pops data from front of the deque
        /// </summary>
        /// <returns>T element</returns>
        public T PopFront()
        {
            MyDoubleLinkedListNode<T> temp = _head;
            T data;

            if (temp.Next != null)
            {
                data = temp.Data;
                temp = temp.Next;
                temp.Prev = null;
                _head = temp;
            }
            else
            {
                data = temp.Data;
                _head = null;
            }

            return data;
        }

        /// <summary>
        /// Pops data from the rear of the deque
        /// </summary>
        /// <returns>T element</returns>
        public T PopRear()
        {
            MyDoubleLinkedListNode<T> temp = _head, prevNode = null;
            T data;

            while (temp.Next != null)
            {
                prevNode = temp;
                temp = temp.Next;
            }

            if (prevNode != null)
            {
                prevNode.Next = null;
                data = temp.Data;
                temp = null;
            }
            else
            {
                data = _head.Data;
                temp = _head = null;
            }

            return data;
        }

        /// <summary>
        /// Prints the values present in the deque on the console
        /// </summary>
        public void Display()
        {
            MyDoubleLinkedListNode<T> temp = _head;

            Console.WriteLine("Displaying elements in the Deque: \r\n");
            while (temp != null)
            {
                Console.WriteLine(temp.Data);
                temp = temp.Next;
            }
        }
        #endregion

        #region Helpers
        /// <summary>
        /// Adds the initial values in deque
        /// </summary>
        /// <param name="a">T[]</param>
        private void createInitialState(T[] a)
        {
            MyDoubleLinkedListNode<T> prevNode = null, currNode = null;

            for (int i = 0; i < a.Length; i++)
            {
                if (i == 0)
                {
                    currNode = new MyDoubleLinkedListNode<T>() { Data = a[i] };
                    _head = currNode;
                }
                else
                {
                    prevNode = currNode;
                    currNode = new MyDoubleLinkedListNode<T>() { Data = a[i] };
                    currNode.Prev = prevNode;
                    prevNode.Next = currNode;
                }
            }
        }

        /// <summary>
        /// Checks and initialized head if not initialized.
        /// </summary>
        /// <param name="data"></param>
        /// <returns>true if head is not initialized</returns>
        private bool isHeadNotInitialized(T data)
        {
            if (_head == null)
            {
                _head = new MyDoubleLinkedListNode<T>(data);
                return true;
            }

            return false;
        }
        #endregion
    }

    /// <summary>
    /// Represents the class for Deque node with data, previous and next node references
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class MyDoubleLinkedListNode<T>
    {
        public MyDoubleLinkedListNode()
        {

        }

        public MyDoubleLinkedListNode(T data)
        {
            this.Data = data;
        }

        public MyDoubleLinkedListNode(MyDoubleLinkedListNode<T> prev, MyDoubleLinkedListNode<T> next, T data) : base()
        {
            this.Next = next;
            this.Prev = prev;
            this.Data = data;
        }

        /// <summary>
        /// Contains data for current node
        /// </summary>
        public T Data { get; set; }

        /// <summary>
        /// Reference for previous node
        /// </summary>
        public MyDoubleLinkedListNode<T> Prev { get; set; }

        /// <summary>
        /// Reference for the next node
        /// </summary>
        public MyDoubleLinkedListNode<T> Next { get; set; }
    }
}
